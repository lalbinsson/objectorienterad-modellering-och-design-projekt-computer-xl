# EDAF60 Project 
*Contributors: Adina Borg, Olivia Mattsson, Lucy Albinsson och Linnéa Gustavsson*

### Instructions:
- [Project instructions](http://fileadmin.cs.lth.se/cs/Education/EDAF60/90b2e863-0494-43ee-aacc-082b447203de/projects/computer/computer.html)
- [Styleguide](http://fileadmin.cs.lth.se/cs/Education/EDAF60/90b2e863-0494-43ee-aacc-082b447203de/java-style-guide.html)

# Inlämningsuppgift 1: Computer

## UML-diagram
![UML](Computer/UML.png)

*1. Det finns ett antal klasser och gränssnitt i programkoden ovan och det behövs ytterligare några för att kunna implementera programmet. Vilka klasser saknas och vilka är gränssnitt respektive klasser? De finns en del av klasser och gränssnitt som inte är implementerade.*

Klasserna är:
- Add
- Mul
- Copy
- Jump
- JumpEq
- Halt
- Print
- ByteWord
- LongWord
- ByteWordFactory
- LongWordFactory
- Address
- Memory
- Program (abstrakt klass)

Och gränssnitten:
- Instructions
- Word
- WordFactory
- BinOp

*2. Klassen Program har till synes metoden add för att lägga till en instruktion till programmet. Vilken standardklass skulle man kunna utvidga för att slippa implementera metoden själv? Är det lämpligt att göra så?*

Vi skulle kunna använda ArrayList eller LinkedList-klasserna och implementera dem. Problemet här är att det skulle medföra väldigt mycket metoder som inte är användbara i vårt program, vilket skulle bryta mot SOLID-egenskapen Interface Segregation Principle - vi vill inte bero på interface och metoder som inte nyttjas. Det skulle vara bättre att göra implementationen själv.

*3. Klasserna skall fördelas på minst två paket. Vilka paket bör finnas och hur fördelas klasserna? (Under föreläsning 7 kommer vi att prata mer om paketindelning, men fundera gärna redan nu igenom vilka klasser som ‘hör ihop’ i detta projekt).*

Vårt förslag är att ha all kod som är grundläggande för att en dator ska fungera i ett separat paket, alltså Computer, Memory, Address, Program, WordFactory och Word i ett.
De som sedan mer är kopplade till programmen och dess utförande kan placeras i ett separat paket.

*4. Studera designmönstret Command. Var och hur bör det användas i uppgiften.*

Då vi har många instruktioner som sköts på liknande sätt kan det användas här för att enkapsulera den informationen vi behöver för att utföra de olika instruktionerna.

*5. Studera designmönstret Template method. Mönstret skall användas för att undvika duplicerad kod i likartade klasser. Var kan detta bli aktuellt?*

Det kommer att nyttjas i exempelvis Progam, där både Factorial och Sum utför instruktioner som är väldigt lika. De kan istället flyttas till Program för att minska upprepad kod.

*6. Studera designmönstret Strategy och exemplen från föreläsningarna. Hur använder man mönstret för att hantera olika sorters operander på ett enhetligt sätt?*

Ett strategy-interface skapas med de nödvändiga metoderna som behöver användas för liknande typer av actions. Exempelvis betala. Om vi erbjuder betalning med ex. kort eller Klarna så kan en metod för att samla in betalningsinformationen, och de båda har varsina klasser(stategy-klasser) som implementerar metoden på olika sätt. Beroende på vilken betalningsform som används så kommer strategy-objekten ha koll på vad som göras, och vårt main-program behöver inte bry sig om vilken metod som valt utan det sköter våra strategies.

*7. När man exekverar Add-instruktionen i exemplen ovan skall man utföra en addition av två tal. I vilken klass skall den faktiska additionen utföras?*

I BinOp-klassen, då det görs där this kallas (om implementationen av BinOp sker på samma sätt som enligt föreläsningarna).

*8. Rita ett sekvensdiagram på papper som visar alla inblandade objekt när Add-kommandot i Factorial exekveras. Ert diagram måste följa våra UML-anvisningar.*

![SEKVENSDIAGRAM](Computer/sekvensdiagram.png)

*9. Vad bör hända om någon gör följande felaktiga anrop?*
run("factorial(5) med olika slags ord", new Factorial("5", lwf), bwf);
(Observera att vi i detta exempel använder olika slags factories när vi skapar programmet och när vi skapar minnet.)

Programmet bör krascha.


# Inlämningsuppgift 2: XL

## UML-diagram
![UML](xl_clean/UML_XL.png)

## Föreberedelseuppgifter
*1. Vilka klasser bör finnas för att representera ett kalkylark?*

I modellen kommer följande klasser behöva läggas till :
- Spreadsheet-klass, skapar hela kalkylarket
- Slot-klass, representerar slot-objekt i kalkylarket. Abstrakt klass
- BlankSlot-klass, representerar en tom ruta i arket. Subklass till Slot
- ExpressionSlot-klass, representerar en slot som innehåller ett uttryck. Subklass till Slot. Får inte hänvisa till en Blank-slot eller till sig själv på något vis.
- CommentSlot-klass, representerar en sträng/kommentar. Subklass till Slot, har värde 0.
- SlotFactory, ansvarig för att skapa slot-objekten
- Address-klass, håller koll på adresserna för slots
- AddressFactory, ansvarig för att skapa address-objekten
(Kommer vi behöva ha olika SlotFactories?)

*2. En ruta i kalkylarket skall kunna innehålla en text eller ett uttryck. Hur modellerar man detta?*

En abstrakt klass som heter Slot. 

*3. Hur skall man hantera uppdragsgivarens krav på minnesresurser?*

En HashMap-implementation av SpreadSheet kan minska minnesförbrukningen vid skapandet av dokumentet. Nyckeln kommer vara addressen och value är då värdet i slot:en.

*4. Vilka klasser skall vara observatörer och vilka skall observeras?*

Modellen ska vara observerbar och vyn ska observera. Controllern kommer addas på Vy-objekten för att hålla koll på när en uppdatering sker och uppdatera modellen efter det.

*5. Vilket paket och vilken klass skall hålla reda på vad som är “Current slot”?*

I Model-paketet i SpreadSheet-klassen.

*6.Vilken funktionalitet är redan färdig och hur fungerar den? Titta på klasserna i view-paketet och testkör.*

Vyn är redan implementerad. 
En del av modellen är också klar, det som skapar uttryck, variabler och beräkningar. Det som behövs kompletteras här är modellens skapande av spreadsheet:et. 
En controller för att förmedla user input behövs också. 

*7. Det kan inträffa ett antal olika fel när man försöker ändra innehållet i ett kalkylark. Då skall undantag kastas. Var skall dessa undantag fångas och hanteras?*

Modellen kommer hantera undantag, då controllern enbart kopplar ihop user input med modellen. Controllern ska vara så “tunn” som möjligt och göra så lite som möjligt på egen hand. 

*8. Vilken klass används för att representera en adress i ett uttryck?*

Address-klassen. 

*9. När ett uttryck som består av en adress skall beräknas används gränssnittet Environment. Vilken klass skall implementera gränssnittet? Varför använder man inte klassnamnet i stället för gränssnittet?*

SpreadSheet, då det kommer bestå av slots som kan vara Environments.


*10. Om ett uttryck i kalkylarket refererar till sig själv, direkt eller indirekt, så kommer det att bli bekymmer vid beräkningen av uttryckets värde. Föreslå något sätt att upptäcka sådana cirkulära beroenden! Det finns en elegant lösning med hjälp av strategimönstret som du får chansen att upptäcka. Om du inte hittar den så kommer handledaren att avslöja den.*

Vi får gärna mer förklaring under redovsiningstillfället. Att koppla strategy-pattern till just detta fallet var klurigt.


## Användningsfall
### Användningsfall 1:
Användaren ska kunna öppna upp sparade XL-filer via menyn. Skriver användaren ett ogiltigt filnamn eller försöker öppna en fil av felaktig typ så kommer programmet att klaga.

### Användningsfall 2:
Användaren ska kunna spara sina XL-filer. Default-namn för filerna är untitled följt av en siffra som stiger desto fler filer som sparats. 

### Användningsfall 3:
Användaren ska kunna klicka på slots i kalkylarket och få upp den markerade slot:en längst upp. Där kan användaren sedan redigera vad som ska stå i rutan. Här ska användaren kunna mata in en kommentar eller ett uttryck. Detta uttryck kan bestå av operationer, andra slots och decimal-siffror. Matar användaren in felaktig input så kommer det att visa fel. En kommentar ska börja med #-tecknet för att anses som en kommentar.

### Användningsfall 4: 
Användaren ska kunna tömma en slot och även kunna tömma alla slots med hjälp av knappar i edit-menyn. Den enskilda slot:en som töms är den som är markerad. Går något fel så ska användaren bli meddelad detta och ombedd att pröva igen.

### Användningsfall 5:
Användaren ska via en Print-knapp i menyn kunna få upp utskriftdialogen. Detta för att kunna skriva ut den aktiva XL-filen. Går något fel så ska användaren bli meddelad detta och ombedd att pröva igen.

### Användningsfall 6:
Användaren ska kunna välja att öppna en ny XL-fil. Det kommer öppnas i ett nytt fönster där samtliga slots är tomma och har default-namnet. Går något fel så ska användaren bli meddelad detta och ombedd att pröva igen.

### Användningsfall 7:
Användaren ska kunna stänga ned den aktiva XL-filen. Om detta görs utan att användaren har sparat innan så kommer allt att tas bort och ingen information sparas. Går något fel så ska användaren bli meddelad detta och ombedd att pröva igen.

### Användningsfall 8:
Användaren ska kunna orientera  sig mellan de olika öppna XL-filerna via meny-fliken Window. Där ska de öppna filerna presenteras i en dropdown-meny. 

### Användningsfall 9:
Efter användaren har skrivit input i den markerade rutan så ska filen uppdateras och det nya värdet ska synas korrekt i vårt spreadsheet. Vid felaktig input så visas det i slot:en.

## Tidsåtgång
Varje gruppmedlem lade ca 20-25h på projekt 2.

