package hardware;

public class LongWord implements Word {
    private long value;

    protected LongWord(long value) {
        this.value = value;
    }

    public void add(Word w1, Word w2) {
        this.value = (long) (((LongWord) w1).value + ((LongWord) w2).value);
    }

    public void mul(Word w1, Word w2) {
        this.value = (long) (((LongWord) w1).value * ((LongWord) w2).value);
    }

    public void copy(Word w) {
        value = (long) ((LongWord) w).value;
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Word)) return false;
        else return value == ((LongWord) other).value;
    }

    @Override
    public Word getWord(Memory memory) {
        return this;
    }

    public String toString() {
        return String.valueOf(value);
    }
}
