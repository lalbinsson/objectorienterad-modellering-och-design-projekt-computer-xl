package hardware;

public class ByteWord implements Word {
    private byte value;

    protected ByteWord(byte value) {
        this.value = value;
    }

    public void add(Word w1, Word w2) {
        this.value = (byte) (((ByteWord) w1).value + ((ByteWord) w2).value);
    }

    public void mul(Word w1, Word w2) {
        this.value = (byte) (((ByteWord) w1).value * ((ByteWord) w2).value);
    }

    public void copy(Word w) {
        value = (byte) ((ByteWord) w).value;
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Word)) return false;
        else return value == ((ByteWord) other).value;
    }

    @Override
    public Word getWord(Memory memory) {
        return this;
    }

    public String toString() {
        return String.valueOf(value);
    }
}
