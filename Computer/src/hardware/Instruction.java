package hardware;

public interface Instruction {

    void execute(Memory memory, ProgramCounter cnt);

    String toString();
}
