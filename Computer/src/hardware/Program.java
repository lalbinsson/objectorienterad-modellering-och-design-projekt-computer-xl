package hardware;

import java.util.ArrayList;
import java.util.List;

public abstract class Program {
    private List<Instruction> instructions;

    public Program() {
        this.instructions = new ArrayList<Instruction>();
    }

    protected void add(Instruction instruction) {
        instructions.add(instruction);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < instructions.size(); i++) {
            sb.append(i + ": " + instructions.get(i).toString() + "\n");
        }
        return sb.toString();
    }

    public Instruction getInstruction(int i){
        return instructions.get(i);
    }

    public boolean isValid(ProgramCounter count){
        return (count.stillRunning() && (count.getIndex() < instructions.size()));
    }
}
