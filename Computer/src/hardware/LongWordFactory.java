package hardware;

public class LongWordFactory implements WordFactory {

    public Word word(String string) {
        return new LongWord(Long.parseLong(string));
    }
}
