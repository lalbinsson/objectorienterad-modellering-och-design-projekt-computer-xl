package hardware;

public class ByteWordFactory implements WordFactory {

    public Word word(String string) {
        return new ByteWord(Byte.parseByte(string));
    }
}
