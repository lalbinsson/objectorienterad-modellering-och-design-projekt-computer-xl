package hardware;

public class Memory {
    private int size;
    private Word[] memory;

    public Memory(int size, WordFactory wf) {
        if (size < 1) {
            throw new IllegalArgumentException("impossible size of memory");
        }
        this.size = size;
        this.memory = new Word[size];
        for (int i = 0; i < size; i++) {
            memory[i] = wf.word("0");
        }
    }

    public Word getWord(int index) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException();
        }
        return memory[index];
    }
}
