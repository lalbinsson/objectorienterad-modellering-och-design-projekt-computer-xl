package hardware;

public interface Operand {

    Word getWord(Memory memory);
}
