package hardware;

public class Computer {
    private Memory memory;
    private Program loadedProgram;

    public Computer(Memory memory) {
        this.memory = memory;
    }

    public void load(Program loadedProgram) {
        this.loadedProgram = loadedProgram;
    }

    public void run() {
        ProgramCounter count = new ProgramCounter();
        while (loadedProgram.isValid(count)) {
            loadedProgram.getInstruction(count.getIndex()).execute(memory, count);
        }
    }
}
