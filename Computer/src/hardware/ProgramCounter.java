package hardware;

public class ProgramCounter {
    private int index;

    public ProgramCounter() {
        this.index = 0;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public void increment() {
        index++;
    }

    public boolean stillRunning() {
        return (index >= 0);
    }

    public void stop(){
        this.index = -1;
    }
}
