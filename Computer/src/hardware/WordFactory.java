package hardware;

public interface WordFactory {

    Word word(String value);
}
