package hardware;

public interface Word extends Operand {

    void add(Word w1, Word w2);

    void mul(Word w1, Word w2);

    void copy(Word w);

    boolean equals(Object other);

    String toString();
}
