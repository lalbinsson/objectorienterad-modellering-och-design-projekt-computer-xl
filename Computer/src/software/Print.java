package software;

import hardware.*;

public class Print implements Instruction {
    protected Address address;

    public Print(Address address) {
        this.address = address;
    }

    @Override
    public void execute(Memory m, ProgramCounter cnt) {
        System.out.println(address.getWord(m).toString());
        cnt.increment();
    }

    @Override
    public String toString() {
        return "Print " + address.toString();
    }
}
