package software;

import hardware.*;

public class Halt implements Instruction {

    public Halt() {}

    @Override
    public void execute(Memory m, ProgramCounter cnt) {
        cnt.stop();
    }

    @Override
    public String toString() {
        return "Halt";
    }
}
