package software;

import hardware.*;

public abstract class JumpOp implements Instruction {
    protected int index;

    public JumpOp(int index) {
        this.index = index;
    }

    protected abstract boolean op(Memory m);

    @Override
    public void execute(Memory m, ProgramCounter cnt) {
        if (op(m)) {
            cnt.setIndex(index);
        } else {
            cnt.increment();
        }
    }

    @Override
    public String toString() {
        return "Jump to " + index + stringOp();
    }

    protected abstract String stringOp();
}
