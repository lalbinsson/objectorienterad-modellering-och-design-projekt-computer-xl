package software;

import hardware.*;

public abstract class BinOp implements Instruction {
    protected Operand o1, o2;
    protected Address address;

    public BinOp(Operand o1, Operand o2, Address address) {
        this.o1 = o1;
        this.o2 = o2;
        this.address = address;
    }

    protected abstract void op(Word w1, Word w2, Word w3);

    @Override
    public void execute(Memory m, ProgramCounter cnt) {
        op(o1.getWord(m), o2.getWord(m), address.getWord(m));
        cnt.increment();
    }

    @Override
    public String toString() {
        return stringOp()
                + " "
                + o1.toString()
                + " and "
                + o2.toString()
                + " into "
                + address.toString();
    }

    protected abstract String stringOp();
}
