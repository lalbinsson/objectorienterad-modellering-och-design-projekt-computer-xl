package software;

import hardware.*;

public class Mul extends BinOp {

    public Mul(Operand o1, Operand o2, Address address) {
        super(o1, o2, address);
    }

    @Override
    protected void op(Word w1, Word w2, Word w3) {
        w3.mul(w1, w2);
    }

    protected String stringOp() {
        return "Mul";
    }
}
