package software;

import hardware.*;

public class Copy implements Instruction {
    protected Operand op;
    protected Address address;

    public Copy(Operand op, Address address) {
        this.op = op;
        this.address = address;
    }

    @Override
    public void execute(Memory m, ProgramCounter cnt) {
        address.getWord(m).copy(op.getWord(m));
        cnt.increment();
    }

    @Override
    public String toString() {
        return "Copy " + op.toString() + " to " + address.toString();
    }
}
