package software;

import hardware.*;

public class Jump extends JumpOp {

    public Jump(int index) {
        super(index);
    }

    @Override
    protected boolean op(Memory m) {
        return true;
    }

    protected String stringOp() {
        return "";
    }
}
