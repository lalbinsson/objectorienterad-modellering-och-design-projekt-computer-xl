package software;

import hardware.*;

public class JumpEq extends JumpOp {
    protected Operand o1, o2;

    public JumpEq(int index, Operand o1, Operand o2) {
        super(index);
        this.o1 = o1;
        this.o2 = o2;
    }

    @Override
    protected boolean op(Memory m) {
        if (o1.getWord(m).equals(o2.getWord(m))) {
            return true;
        } else {
            return false;
        }
    }

    protected String stringOp() {
        return " if " + o1.toString() + " == " + o2.toString();
    }
}
