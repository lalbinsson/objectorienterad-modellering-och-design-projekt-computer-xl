package software;

import hardware.*;

public class Add extends BinOp {

    public Add(Operand o1, Operand o2, Address address) {
        super(o1, o2, address);
    }

    @Override
    protected void op(Word w1, Word w2, Word w3) {
        w3.add(w1, w2);
    }

    protected String stringOp() {
        return "Add";
    }
}
