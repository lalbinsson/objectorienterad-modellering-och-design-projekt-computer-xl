package util;

public class XLException extends RuntimeException {
    public static final XLException CIRCULAR_ERROR = new XLException("Found a dependency. Please enter a new value");
    public static final XLException BUILDINGSLOT_ERROR = new XLException("Not possible to create cell");
    public static final XLException NO_VALUE = new XLException("No value in this slot");
    public static final XLException DEPENDENCY_SLOT = new XLException(
            "This slot cannot be removed, since another slot depends on it. First remove dependencies.");
    public static final XLException LOAD_ERROR = new XLException(
            "Unable to load the file. Please check if the file is valid");

    public XLException(String message) {
        super(message);
    }
}