package gui;

import java.awt.Color;
import java.util.Observable;

public class CurrentLabel extends ColoredLabel implements java.util.Observer {
    CurrentSlot current;

    public CurrentLabel(CurrentSlot current) {
        super("A1", Color.WHITE);
        this.current = current;
        this.current.addObserver(this);
    }

    @Override
    public void update(Observable obs, Object obj) {
        setText(((CurrentSlot) obs).getAddress());

    }
}