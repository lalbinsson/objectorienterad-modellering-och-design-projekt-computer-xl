package gui;

import java.awt.Color;
import javax.swing.JTextField;
import java.util.Observable;
import java.util.Observer;
import java.util.Optional;

import gui.CurrentSlot;
import model.Slot;
import model.SpreadSheet;

public class Editor extends JTextField implements Observer {
    private CurrentSlot current;
    private SpreadSheet sheet;

    public Editor(SpreadSheet sheet, CurrentSlot current) {
        this.current = current;
        this.sheet = sheet;
        setBackground(Color.WHITE);
        current.addObserver((Observer) this);
        addActionListener(e -> {
            String value = getText();
            if (!value.isEmpty()) {
                sheet.addSlot(value, current.getAddress());
            } else {
                sheet.clearSlot(current.getAddress());
            }
            sheet.clearStatus();
        });
    }

    @Override
    public void update(Observable obs, Object o) {
        Optional<Slot> opSlot = sheet.getSlot(current.getAddress());
        if (opSlot.isPresent()) {
            opSlot.ifPresent(slot -> setText(slot.toString()));
        } else {
            setText("");
        }
    }
}