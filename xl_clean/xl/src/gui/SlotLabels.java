package gui;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.TreeMap;

import javax.swing.SwingConstants;

import model.CommentSlot;
import model.Slot;
import model.SpreadSheet;

public class SlotLabels extends GridPanel implements Observer {
    private List<SlotLabel> labelList;
    private SpreadSheet sheet;

    public SlotLabels(int rows, int cols, CurrentSlot current, SpreadSheet sheet) {
        super(rows + 1, cols);
        labelList = new ArrayList<SlotLabel>(rows * cols);
        this.sheet = sheet;
        sheet.addObserver(this);
        for (char ch = 'A'; ch < 'A' + cols; ch++) {
            add(new ColoredLabel(Character.toString(ch), Color.LIGHT_GRAY, SwingConstants.CENTER));
        }
        for (int row = 1; row <= rows; row++) {
            for (char ch = 'A'; ch < 'A' + cols; ch++) {
                String address = ch + String.valueOf(row);
                SlotLabel label = new SlotLabel(address);
                label.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mousePressed(MouseEvent e) {
                        int index = labelList.indexOf(label);
                        int row = index / cols + 1;
                        char col = (char) (index % cols + 'A');
                        String address = String.valueOf(col) + String.valueOf(row);
                        current.setNewCurrent(label, address);
                    }
                });
                add(label);
                labelList.add(label);
            }
        }
        current.setNewCurrent(labelList.get(0), "A1");
    }

    @Override
    public void update(Observable obs, Object obj) {
        Map<String, Slot> temp = sheet.getSlots();
        for (SlotLabel label : labelList) {
            if (!temp.containsKey(label.getAddress())) {
                label.setText("");
            } else {
                Slot sheetSlot = temp.get(label.getAddress());
                if (sheetSlot instanceof CommentSlot) {
                    String comment = sheetSlot.toString();
                    label.setText(comment.substring(1));
                } else {
                    label.setText(Double.toString(sheet.value(label.getAddress())));
                }
            }
        }
    }

}
