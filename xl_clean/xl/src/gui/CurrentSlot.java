package gui;

import java.awt.Color;
import java.util.Observable;

public class CurrentSlot extends Observable {
    private String address;
    private SlotLabel label;

    public String getAddress() {
        return address;
    }

    public void setNewCurrent(SlotLabel newLabel, String address) {
        if (label != null) {
            label.setBackground(Color.WHITE);
        }
        this.label = newLabel;
        this.address = address;
        label.setBackground(Color.YELLOW);
        setChanged();
        notifyObservers();
    }

}
