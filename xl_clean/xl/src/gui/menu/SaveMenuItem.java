package gui.menu;

import gui.StatusLabel;
import gui.XL;
import java.io.FileNotFoundException;
import javax.swing.JFileChooser;
import model.*;

class SaveMenuItem extends OpenMenuItem {
    private SpreadSheet slots;

    public SaveMenuItem(XL xl, StatusLabel statusLabel) {
        super(xl, statusLabel, "Save");
        this.slots = xl.getSheet();
    }

    protected void action(String path) throws FileNotFoundException {
        try {
            slots.saveToFile(path);
        } catch (FileNotFoundException e) {
            statusLabel.setText(e.getMessage());
            e.printStackTrace();
        }
        }

    protected int openDialog(JFileChooser fileChooser) {
        return fileChooser.showSaveDialog(xl);
    }
}