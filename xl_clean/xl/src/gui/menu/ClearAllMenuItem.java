package gui.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuItem;
import model.SpreadSheet;

class ClearAllMenuItem extends JMenuItem implements ActionListener {
    private SpreadSheet sheet;

    public ClearAllMenuItem(SpreadSheet sheet) {
        super("Clear all");
        this.sheet = sheet;
        addActionListener(this);
    }

    public void actionPerformed(ActionEvent e) {
        sheet.clearAllSlots();
    }
}