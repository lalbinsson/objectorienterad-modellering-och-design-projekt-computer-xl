package gui.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuItem;
import gui.CurrentSlot;
import model.SpreadSheet;

class ClearMenuItem extends JMenuItem implements ActionListener {
    private CurrentSlot currentSlot;
    private SpreadSheet slots;

    public ClearMenuItem(CurrentSlot current, SpreadSheet slots) {
        super("Clear");
        this.slots = slots;
        this.currentSlot = current;
        addActionListener(this);
    }

    public void actionPerformed(ActionEvent e) {
        slots.clearSlot(currentSlot.getAddress());
    }
}