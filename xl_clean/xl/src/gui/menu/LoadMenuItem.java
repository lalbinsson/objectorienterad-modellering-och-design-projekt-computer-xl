package gui.menu;

import gui.StatusLabel;
import gui.XL;
import model.SpreadSheet;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JFileChooser;

class LoadMenuItem extends OpenMenuItem {

    public LoadMenuItem(XL xl, StatusLabel statusLabel) {
        super(xl, statusLabel, "Load");
    }

    protected void action(String path) {
        statusLabel.clearStatus();
        xl.load(path);
    }

    protected int openDialog(JFileChooser fileChooser) {
        return fileChooser.showOpenDialog(xl);
    }
}