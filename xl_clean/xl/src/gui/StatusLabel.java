package gui;

import java.awt.Color;
import java.util.Observable;
import java.util.Observer;

import model.SpreadSheet;

public class StatusLabel extends ColoredLabel implements Observer {
    private SpreadSheet sheet;

    public StatusLabel(SpreadSheet sheet) {
        super("", Color.WHITE);
        this.sheet = sheet;
        sheet.addObserver(this);
    }

    @Override
    public void update(Observable observable, Object object) {
        setText(sheet.getStatus());
    }

    public void clearStatus() {
        setText("");

    }
}