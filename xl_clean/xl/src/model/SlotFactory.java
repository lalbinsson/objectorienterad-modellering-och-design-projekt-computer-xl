package model;

import java.io.IOException;

import expr.Expr;
import expr.ExprParser;
import util.XLException;

public class SlotFactory implements Factory {
    private ExprParser parser;

    public SlotFactory() {
        this.parser = new ExprParser();
    }

    @Override
    public Slot create(String command) {
        if (command.startsWith("#")) {
            return new CommentSlot(command);
        } else {
            try {
                Expr expr = parser.build(command);
                return new ExprSlot(expr);
            } catch (IOException e) {
                throw XLException.BUILDINGSLOT_ERROR;
            }
        }
    }

}
