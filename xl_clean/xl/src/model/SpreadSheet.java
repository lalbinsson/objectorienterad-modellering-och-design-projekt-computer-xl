package model;

import expr.Environment;
import util.XLException;

import java.util.TreeMap;
import java.util.Map;
import java.util.Observable;
import java.util.Optional;
import java.util.Set;
import java.io.FileNotFoundException;
import java.io.IOException;

public class SpreadSheet extends Observable implements Environment {
    private SlotFactory slotFactory;
    private Map<String, Slot> slots;
    private String status;

    public SpreadSheet() {
        this.slots = new TreeMap<>();
        this.slotFactory = new SlotFactory();
        this.status = "";
    }

    @Override
    public double value(String address) {
        return getSlot(address).map(slot -> slot.getValue(this)).orElse(0.0);
    }

    public Optional<Slot> getSlot(String address) {
        return Optional.ofNullable(slots.get(address));
    }

    public void clearSlot(String address) {
        Slot tempSlot = slots.get(address);
        try {
            slots.remove(address);
            setUpdated();
        } catch (XLException e) {
            slots.put(address, tempSlot);
            status = e.toString();
        }
        setUpdated();
    }

    public void clearAllSlots() {
        slots.clear();
        setUpdated();
    }

    public void setUpdated() {
        setChanged();
        notifyObservers();
    }

    public Map<String, Slot> getSlots() {
        return slots;
    }

    public void addSlot(String command, String address) {
        Slot newSlot = slotFactory.create(command);
        if (validate(command, address)) {
            try {
                slots.put(address, newSlot);
            } catch (NullPointerException e) {
                status = e.toString();
            }
        }
        setUpdated();
    }

    public boolean validate(String command, String address) {
        Slot oldSlot = slots.get(address);
        Slot newSlot = slotFactory.create(command);
        CircularSlot circular = new CircularSlot();
        slots.put(address, circular);
        try {
            newSlot.getValue(this);
        } catch (XLException | NullPointerException e) {
            status = e.toString();
            slots.put(address, oldSlot);
            setUpdated();
            return false;
        }
        slots.put(address, oldSlot);
        return true;
    }

    public String getStatus() {
        return status;
    }

    public void clearStatus() {
        status = "";
    }

    public void saveToFile(String path) throws FileNotFoundException {
        XLPrintStream print = new XLPrintStream(path);
        print.save(slots.entrySet());
        print.close();
        setUpdated();
    }

    public void load(String path) {
        try {
            XLBufferedReader xlBufferedReader = new XLBufferedReader(path, slotFactory);
            xlBufferedReader.load(slots);
            Set<Map.Entry<String, Slot>> set = slots.entrySet();
            for (Map.Entry<String, Slot> entry : set) {
                slots.put(entry.getKey(), entry.getValue());
            }
            xlBufferedReader.close();
            status = "";
        } catch (XLException | IOException e) {
            status = e.toString();
        }
        setUpdated();
    }

}