package model;

public interface Factory {

    public Slot create(String command);
}
