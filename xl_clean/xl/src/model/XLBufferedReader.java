package model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import util.XLException;

public class XLBufferedReader extends BufferedReader {
    private SlotFactory factory;

    public XLBufferedReader(String name, SlotFactory factory) throws FileNotFoundException {
        super(new FileReader(name));
        this.factory = factory;
    }

    public void load(Map<String, Slot> map) {
        try {
            while (ready()) {
                String string = readLine();
                String[] index = string.split("=");
                String label = index[0];
                String expr = index[1];
                Slot slot = factory.create(expr);
                map.put(label, slot);
            }
        } catch (XLException | IOException e) {
            throw XLException.LOAD_ERROR;
        }
    }
}
