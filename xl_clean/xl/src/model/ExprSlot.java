package model;

import expr.Environment;
import expr.Expr;

public class ExprSlot implements Slot {
    private Expr expr;

    public ExprSlot(Expr expr) {
        this.expr = expr;
    }

    @Override
    public double getValue(Environment env) {
        return expr.value(env);
    }

    @Override
    public String toString() {
        return expr.toString();
    }
}
