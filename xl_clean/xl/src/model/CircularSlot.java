package model;

import expr.Environment;
import util.XLException;

public class CircularSlot implements Slot {

    @Override
    public double getValue(Environment env) throws XLException {
        throw XLException.CIRCULAR_ERROR;
    }

}
