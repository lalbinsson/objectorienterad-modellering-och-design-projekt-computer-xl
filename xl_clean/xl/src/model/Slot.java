package model;

import expr.Environment;

public interface Slot {

    double getValue(Environment env);

    String toString();
}
